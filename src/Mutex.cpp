/*******************************************************************************
 *
 * File: Mutex.cpp
 *	Generic Software Operating System Interface
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "gsinterfaces/Mutex.h"
#include <stdio.h>

namespace gsi
{
	// When changing to the C++11 implementation, all implementation of these 
	// classes was moved to the header file
	uint8_t g_mutex_unused; // prevents link warning

} // namespace gsi
