# Generic Software Interfaces

This is a collection of classes intended to hide the differences between operating systems, it is often called an Operating System Abstraction Layer (OSAL).

The development environment should specify one of the OS indicators to make this library compile correctly. 
If using CMake it is suggested that all compiler dependant settings (including this) get put in a toolchain file
 - _WINDOWS
 - _APPLE
 - _LINUX

The intent is to move any reusable utility type source that has a #ifdef <OS> tag in it, into this library.

This library has three folders
 - include - for all include files (.h, .hpp)
 - src - for all source files (.c, .cpp)
 - test - for any files (include, source, data) that are used for testing the classes and interfaces provided by this library